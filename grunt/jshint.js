module.exports={
    options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        forin: true,
        globals: {
            jQuery: true
        }
    },
    src: [
        'src/main/webapp/assets/js/**/*.js',
        '!src/main/webapp/assets/js/analytics.js'
    ],
    cfg: [
        'grunt/**/*.js',
        'Gruntfile.js'
    ],
    test: [
        'src/test/javascript/tests/**/*.js'
    ]
};