module.exports = {

    test: {
        options: {
            port: 1337,
            hostname: "localhost",
            bases: 'src/main/webapp',
            serverreload: false
        }

    },
    keepalive: {
        options: {
            port: 1337,
            hostname: "localhost",
            bases: 'src/main/webapp',
            serverreload: true
        }
    }

} ;