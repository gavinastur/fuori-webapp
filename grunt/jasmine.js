module.exports = {

    test: {
        src : 'src/main/webapp/assets/**/*.js',
        options: {
            keepRunner: true,
            summary: true,
            specs: 'src/test/javascript/tests/specs/**/*.js',
            template: require('grunt-template-jasmine-istanbul'),
            templateOptions: {
                coverage: 'src/test/javascript/reports/coverage/coverage.json',
                report: [
                    {
                        type: 'lcov',
                        options: {
                            dir: 'src/test/javascript/reports/coverage/'
                        }
                    },
                    {
                        type: 'json',
                        options: {
                            dir: 'src/test/javascript/reports/coverage/'
                        }
                    },
                    {
                        type: 'text-summary'
                    }
                ],

                replace: false,
                template: require('grunt-template-jasmine-requirejs'),
                templateOptions: {
                    requireConfig: {
                        paths: {
                            'jasmine-jquery': 'bower_components/jasmine-jquery/lib/jasmine-jquery'
                        },
                        baseUrl: 'src/main/webapp',
                        callback: function () {
                            define('', ['module'], function (module) {
                                return module.config().src;
                            });
                            var oldLoad = requirejs.load;
                            requirejs.load = function (context, moduleName, url) {
                                //Redirect to the instrumented src for our custom js
                                if (url.indexOf('src/main/webapp/assets') > -1) {
                                    url = './.grunt/grunt-contrib-jasmine/' + url;
                                }
                                return oldLoad.apply(this, [context, moduleName, url]);
                            };
                        }

                    },
                    requireConfigFile: 'src/main/webapp/assets/js/app.js'
                }
            }
        }

    }

};