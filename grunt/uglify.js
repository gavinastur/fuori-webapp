module.exports={
    js: {
        files: [
            {
                expand: true,
                cwd: 'src/main/webapp/assets/',
                src: '**/*.js',
                dest: 'src/main/webapp/assets/'
            }
        ]
    }
};