module.exports = {

    options: {
        spawn: true,
        livereload: true
    },

    styles: {
        files: [
            'src/main/webapp/less/*.less'
        ],
        tasks: [
            'less'
        ]
    },

    html: {
        files: ['*.html', 'src/main/webapp/assets/templates/*.html', 'src/main/webapp/*.html', 'src/main/webapp/assets/templates/partials/*.html'],
        options: {
            livereload: true
        }
    },

    js: {
        files: ['src/main/webapp/assets/js/*.js'],
        tasks: [
            'jshint'
        ],
        options: {
            livereload: true,
        }
    }
};