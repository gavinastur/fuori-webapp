module.exports = {
    development: {
        options: {
            paths: ['src/main/webapp/assets/css'],
            cleancss: true,
            compress: true
        },
        files: {
            'src/main/webapp/assets/css/fuori.css': 'src/main/webapp/less/fuori.less'
        }
    }
};