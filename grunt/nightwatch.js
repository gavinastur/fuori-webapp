module.exports = {
    options: {
        // task options
        standalone: true,

        // download settings
        jar_version: '2.53.0',
        jar_path: 'node_modules/selenium-server/lib/runner/selenium-server-standalone-2.53.1.jar',
        jar_url: '',

        src_folders: ['src/test/javascript/tests/e2e'],
        output_folder: 'src/test/javascript/reports',
        "test_settings" : {
            "default": {
                "desiredCapabilities": {
                    "browserName": "phantomjs"
                }
            },
            "local" : {
                "launch_url" : "http://localhost:1337"
            }
        },


        "phantomjs": {
            "desiredCapabilities": {
                "browserName": "phantomjs",
                "javascriptEnabled": true,
                "acceptSslCerts": true,
                "phantomjs.binary.path": "node_modules/phantomjs/bin"
            }
        },
        "chrome": {
            "desiredCapabilities": {
                "browserName": "chrome",
                "javascriptEnabled": true,
                "acceptSslCerts": true,
                "phantomjs.binary.path": "node_modules/chromedriver/bin"
            }
        }

    }

};