module.exports={
    options: {
        limit: 3
    },
    defaults: [
        ['less'],
        //'json_schema',
        'jshint'
    ],
    devFirst: [

    ],
    devSecond: [
        'jasmine',
        ['express:test','nightwatch:local']
    ],
    prodFirst: [
        'uglify'
    ],
    prodSecond: [
        'jasmine',
        'nightwatch:local'
    ]
};