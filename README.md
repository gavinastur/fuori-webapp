# JavaScript project built with #

Npm - package manager (Requires Node installed globally)

Bower - dependency versions

Grunt - the build tool (installed globally)

##Simply clone the repo then run 

> npm start

This will run a complete build including 


 `Linting`


 `Unit Tests (Jasmine)`


 `E2ETests (NightwatchJS)`
 

`Unit Test Coverage report`

##Running locally 

> grunt express

You'll be able to access the site via http://localhost:1337/

##Running in Docker
I've not Dockerised the build yet but I will add details shortly