define(['text!assets/cms/json/meta.json'], function(json){

    var _metadata = JSON.parse(json);

    var getData = function(idx) {
        //query the JSON for our page
        return _metadata.meta.pages.filter(function(item) { return item.id === idx; });
    };

    var getAll = function() {
        return _metadata.meta;
    };

    return {
        getData: getData,
        getAll: getAll
    };
});