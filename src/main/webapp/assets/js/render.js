define(['jquery','ractive','assets/js/templates', 'assets/js/lang'], function($,Ractive,Template,Lang){

    Ractive.DEBUG = false;

    var loadData = function(container) {
        Lang.getData(function (_data) {
            container.set(_data);
            console.log(_data);
        });
    };

    // Outer Containers
    var home = function() {

        // Outer Containers
        var mainContainer = $('[data-template-container="index"]');
        if (mainContainer[0]) {
            var mainContainerRactive = new Ractive({
                el: mainContainer,
                data: {},
                template: Template.main,
                partials: {
                    nav: Template.nav,
                    header: Template.header,
                    about: Template.about,
                    skills: Template.skills,
                    contact: Template.contact,
                    footer: Template.footer
                },
                oncomplete: function() {
                    loadData(mainContainerRactive);

                    $(function() {
                        $('body').on('click', '.page-scroll a', function(event) {
                            var $anchor = $(this);
                            $('html, body').stop().animate({
                                scrollTop: $($anchor.attr('href')).offset().top
                            }, 1500, 'easeInOutExpo');
                            event.preventDefault();
                        });
                    });

                    var headerContainer = $('.navbar-fixed-top');
                    var headerContainerPos = headerContainer.offset().top;

                    var scrollerFn = function(event) {
                        var ev = event;
                        if (event === 'scroll') {
                            scrollerFn.position();
                        }
                        else if (event === 'resize') {
                            console.log('Resized');
                            scrollerFn.resize();
                        }
                    };

                    scrollerFn.position = function() {
                        if ( $(window).scrollTop() > headerContainerPos ) {
                            headerContainer.addClass('navbar-shrink');
                        } else if ($(window).scrollTop() <= headerContainerPos ) {
                            headerContainer.removeClass('navbar-shrink');
                        }
                    };


                    $(document).scroll(function(ev) {
                        scrollerFn('scroll');
                    });


                    // Highlight the top nav as scrolling occurs
                    $('body').scrollspy({
                        target: '.navbar-fixed-top'
                    });

                    // Closes the Responsive Menu on Menu Item Click
                    $('.navbar-collapse ul li a').click(function() {
                        $('.navbar-toggle:visible').click();
                    });
                }
            });


        }

    };

    return {home: home};
});