define(['jquery'], function($){

    var data = {
        lang: 'en'
    };

    var getLang = function getLang() {
        return data.lang;
    };

    var setLang = function setLang(lang) {
        data.lang = lang;
    };


    var getData = function (callback){
        $.getJSON('assets/cms/json/'+ getLang() +'.json', function(data){
            callback(data);
        });
    };

    return {
        getData: getData,
        getLang: getLang,
        setLang: setLang
    };
});
