var templatePath = 'assets/templates/';

define([
    'text!' + templatePath + 'main.html',
    'text!' + templatePath + 'partials/nav.html',
    'text!' + templatePath + 'partials/header.html',
    'text!' + templatePath + 'partials/skills.html',
    'text!' + templatePath + 'partials/about.html',
    'text!' + templatePath + 'partials/contact.html',
    'text!' + templatePath + 'partials/footer.html'

],
    function(main, nav, header, skills, about, contact, footer) {

        return {
            "main" : main,
            "nav" : nav,
            "header" : header,
            "skills" : skills,
            "about" : about,
            "contact" : contact,
            "footer" : footer
        };

    });

