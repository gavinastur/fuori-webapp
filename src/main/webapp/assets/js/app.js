require.config({
    baseUrl: '',
    paths: {
        jquery: 'bower_components/jquery/dist/jquery.min',
        jscookie: 'bower_components/js-cookie/src/js.cookie',
        jqueryeasing: 'bower_components/jquery.easing/js/jquery.easing.min',
        ractive: 'bower_components/ractive/ractive.min',
        bootstrap: 'bower_components/bootstrap/dist/js/bootstrap.min',
        text: 'bower_components/text/text',
        render: 'assets/js/render',
        cookies: 'assets/js/cookies'
    },
    urlArgs: "v=1",
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'jqueryeasing': {
            deps: ['jquery']
        },
        'cookies': {
            deps: ['jquery','jscookie']
        }

}
});
require([
    'jquery',
    'jqueryeasing',
    'ractive',
    'bootstrap',
    'render',
    'cookies'
],
    function(
        $,
        $je,
        Ractive,
        Bootstrap,
        Render,
        Cookies
        ) {

       Render.home();
       Cookies.init();

    });
