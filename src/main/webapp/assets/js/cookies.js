define(['jquery', 'jscookie', 'ractive','text!assets/js/analytics.js'], function($, $cookie, Ractive, atemplate){

    var _isCookieAccepted = function(name) {
        return $cookie.get(name) === name;
    };

    var _addCookie = function(options) {
        $cookie.set(options.name, options.name, {
            expires: options.expires,
            path: options.path
        });
    };

    var _enableAnalytics = function() {

        var anaContainer = $('[data-template-container="analytics"]');

        if (anaContainer[0]) {

            new Ractive({
                el: anaContainer,
                data: {},
                template: atemplate
            });
        }
    };

    var init = function() {

        var cookieOptions = {
            name: '_fuori',
            expires: 365,
            path: '/'
        };

        if (!_isCookieAccepted(cookieOptions.name)) {

            $(".cookie-policy").fadeIn();

            $('button.close').click(function(e) {
                e.preventDefault();

                _addCookie(cookieOptions);

                _enableAnalytics();

                $(".cookie-policy").fadeOut();
            });
        } else {
            _enableAnalytics();
        }

    };

    return {
        init: init
    };
});