define(['jasmine-jquery','bootstrap','jquery','ractive','assets/js/render','assets/js/templates', 'assets/js/lang'], function($j,bootstrap,$,Ractive,Render,Template,Lang) {

    Ractive.DEBUG = false;

    describe("Render Home", function() {

        beforeEach(function() {
            setFixtures('<body id="page-top" class="index"><nav class="navbar navbar-default navbar-fixed-top"></nav><main data-template-container="index"></main></body>');
        });

        it("should query main container when the home page is rendered", function() {

            var spy = spyOn($.fn, 'find').and.callThrough();

            Render.home();

            expect(spy).toHaveBeenCalledWith('[data-template-container="index"]');
        });


    });


});
