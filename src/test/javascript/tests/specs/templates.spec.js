define(['assets/js/templates', 'ractive'], function(Templates, Ractive) {

    var mockdata = {
        "copyright": "Copyright text"
    };

    describe("Templates", function() {

        it("should return a Footer template", function() {
            expect(Templates.footer).toContain('{{copyright}}');
        });

        it("should return a Main template", function() {
            expect(Templates.main).toContain('{{>nav}}');
            expect(Templates.main).toContain('{{>header}}');
            expect(Templates.main).toContain('{{>about}}');
            expect(Templates.main).toContain('{{>skills}}');
            expect(Templates.main).toContain('{{>footer}}');
        });

        it("should return a populated Footer template", function() {
            Ractive.DEBUG = false;
            var footer = new Ractive({
                template: Templates.footer,
                data: mockdata
            });

            expect(footer.toHTML()).toContain('<footer class="text-center"><div class="footer-above"><div class="container"><div class="row"></div></div></div> <div class="footer-below"><div class="container"><div class="row"><div class="col-lg-12">Copyright text <br> </div></div></div></div></footer>');
        });
    });
});
