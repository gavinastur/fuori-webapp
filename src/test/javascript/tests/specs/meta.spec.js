define(['assets/js/meta'], function(Meta) {

    describe("Meta", function() {

        it("should retrieve Home Page meta", function() {
            expect(Meta.getData('home').length).toEqual(1);
        });

        it("should retrieve all Meta data", function() {
            expect(Meta.getAll().pages.length).toEqual(4);
        });

        it("should retrieve no Meta data for page id 'doesnotexist'", function() {
            expect(Meta.getData('doesnotexist').length).toEqual(0);
        });

    });
});
