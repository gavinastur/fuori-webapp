define(['jasmine-jquery','jquery','jscookie','assets/js/cookies'], function(J,$,$Cookie,Cookies) {

    describe("Cookies init", function() {

        beforeEach(function() {

            $Cookie.remove('_fuori');
            setFixtures('<script data-template-container="analytics"></script><div class="cookie-policy" style="display:none;"><button class="close"></button></div>');
        });

        it("should query site-cookie and fade in message when cookie not accepted", function() {

            var fadeInSpy = spyOn($.prototype, 'fadeIn');
            var findSpy = spyOn($.fn, 'find').and.callThrough();

            Cookies.init();

            expect(findSpy).toHaveBeenCalledWith('.cookie-policy');
            expect(fadeInSpy).toHaveBeenCalled();

        });

        it("should query site-cookie and fade out message when cookie accepted", function() {

            var findSpy = spyOn($.fn, 'find').and.callThrough();
            var clickEventSpy = spyOnEvent('button.close','click');
            var fadeOutSpy = spyOn($.prototype, 'fadeOut');

            Cookies.init();

            expect(findSpy).toHaveBeenCalledWith('.cookie-policy');

            $('button.close').click();

            expect('click').toHaveBeenTriggeredOn('button.close');
            expect(clickEventSpy).toHaveBeenTriggered();
            expect(fadeOutSpy).toHaveBeenCalled();

        });

    });
});
