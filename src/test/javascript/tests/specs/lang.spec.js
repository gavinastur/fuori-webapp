define(['assets/js/lang', 'jquery'], function(Lang, $) {

    describe("Lang", function() {

        it("should return default lang 'en'", function() {
            expect(Lang.getLang()).toEqual('en');
        });

        it("should update lang to 'fr'", function() {
            Lang.setLang('fr');
            expect(Lang.getLang()).toEqual('fr');
        });

    });

    describe('Lang async', function () {

        beforeEach(function() {
            spyOn($, 'getJSON').and.callFake(function(url,cb) {
                cb({
                    "test": "A value"
                });
            });
        });

        it("should test Lang.getData envokes a callback", function () {

            var cb = jasmine.createSpy();

            Lang.getData(cb);
            expect(cb).toHaveBeenCalled();

        });

        it("should test Lang.getData envokes a callback with some dummy data", function () {

            var cb = function(data) {
                expect(data.test).toEqual('A value');
            };

            Lang.getData(cb);

        });


    });
});
