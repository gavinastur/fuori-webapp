(function () {
   "use strict";
    module.exports = {

        beforeEach : function(browser, cb) {
            console.log('Running against: ' + browser.launch_url);
            cb();
        },

        'Check Home': function (browser) {
            browser
                .url(browser.launch_url + '/index.html')
                .waitForElementVisible('body', 1000)
                .waitForElementVisible('main[data-template-container=index]', 1000)
                .assert.elementPresent('nav')
                .assert.elementPresent('header')
                .assert.elementPresent('section#about')
                .assert.elementPresent('.logo > h1')
                .assert.elementPresent('section#skills')
                .assert.elementPresent('#contact a[href="mailto:hello@fuori.co.uk"]')
                .end();

        },

        'Check .navbar-toggle hidden on desktop': function (browser) {
            browser
                .resizeWindow(992, 900)
                .url(browser.launch_url + '/index.html')
                .waitForElementVisible('body', 1000)
                .assert.hidden('.navbar-toggle')
                .end();
        },

        'Check .navbar-toggle visible on mobile': function (browser) {
            browser
                .url(browser.launch_url + '/index.html')
                .resizeWindow(360, 900)
                .waitForElementVisible('body', 1000)
                .waitForElementVisible('.navbar-toggle', 1000)
                .assert.visible('.navbar-toggle')
                .end();
        },

        'Cookie policy - check policy visible': function (browser) {
            browser
                .url(browser.launch_url + '/index.html')
                .waitForElementVisible('body', 1000)
                .waitForElementVisible('.cookie-policy', 1000)
                .assert.elementPresent('button.close')
                .end();

        },

        'Cookie policy - check policy not visible when cookies accepted': function (browser) {
            browser
                .url(browser.launch_url + '/index.html')
                .waitForElementVisible('body', 1000)
                .waitForElementVisible('.cookie-policy', 1000)
                .click('button.close').pause(2000)
                .assert.hidden('.cookie-policy')
                .end();
        },

        'Cookie policy - check cookie values when accepted': function (browser) {

            var cookiename = '_fuori';
            //Accept the cookie
            browser
                .url(browser.launch_url + '/index.html')
                .waitForElementVisible('body', 1000)
                .waitForElementVisible('.cookie-policy', 1000)
                .click('button.close').pause(2000)
                .assert.hidden('.cookie-policy');

            //Verify the cookie values
            browser.getCookie(cookiename, function callback(cookie) {
                this.assert.equal(cookie.value, '_fuori');
                this.assert.equal(cookie.name, '_fuori');
            });

            //Verify the cookie policy doesn't reappear
            browser
                .url(browser.launch_url + '/index.html')
                .waitForElementVisible('body', 1000)
                .assert.hidden('.cookie-policy')
                .end();
        }
    };
}());
